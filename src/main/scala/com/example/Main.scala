package com.example

import java.util.concurrent.Executors

import scala.language.postfixOps
import scala.collection.concurrent.TrieMap
import scala.concurrent.{
  Await,
  ExecutionContext,
  ExecutionContextExecutor,
  ExecutionContextExecutorService,
  Future
}
import scala.concurrent.duration._
import scala.util.Random

object Main {
  def main(args: Array[String]): Unit = {
    // URL website to parse
    val baseUrl: String = "https://www.apkmirror.com"
    // Number of download URLs to parse
    val maxNum: Int = 20000
    // Text filename for saving download links
    val apkFilename = "apk_urls.txt"
    // Number of crawlers threads
    val numTasks = 4
    // Waiting time for the job of saving temporary url text file
    val urlTextFileSleepTime = (3 minutes).toMillis
    // Waiting time for updating download links in the url text file
    val updateLinksSleepTime = (1 minutes).toMillis
    // If set to true, will start downloading found download URLs
    val downloadFilesFlag = true

    // Declaring objects
    val crawler = Crawler(baseUrl, maxNum)
    val fileDriver = FileDriver()

    // Map with (application URL -> download URL)
    var apkMap: scala.collection.concurrent.TrieMap[String, String] =
      fileDriver.readTextFile(apkFilename)
    // Map for storing visited URLs
    var crawledMap: scala.collection.concurrent.TrieMap[String, Boolean] =
      TrieMap[String, Boolean]()

    // Execution context for crawlers threads
    implicit val ec: ExecutionContextExecutorService =
      ExecutionContext.fromExecutorService(
        Executors.newFixedThreadPool(numTasks)
      )
    val crawlingTasksFutures = (1 to numTasks)
      .map(x => x * Random.between(10, 100))
      .map(x => s"https://www.apkmirror.com/page/$x/")
      .map(x => Future { crawler.crawlUrls(x, crawledMap, apkMap) }(ec))

    // Execution context for updating URLs text files
    val fileOpsExecutor =
      ExecutionContext.fromExecutorService(Executors.newFixedThreadPool(2))
    val apkDownloadLinkUpdater: Future[Unit] = Future {
      crawler.updateDownloadLinks(
        apkMap,
        updateLinksSleepTime,
        fileDriver,
        apkFilename
      )
    }(fileOpsExecutor)
    val apkMapWriter: Future[Unit] = Future {
      crawler.saveApkMap(apkFilename, apkMap, fileDriver, urlTextFileSleepTime)
    }(fileOpsExecutor)

    // Crawling the website and saving the temporary results
    try {
      Await.result(Future.sequence(crawlingTasksFutures), 1 hour)
      Await.result(
        Future.sequence(List(apkDownloadLinkUpdater, apkMapWriter)),
        1 hour
      )
    } finally {
      ec.shutdown()
      fileOpsExecutor.shutdownNow()
    }

    // Output result download link in format (App page URL -> Download Link)
    println(apkMap)

    // Download files if downloadFilesFlag set to true
    val fileDownloader = FileDownloader()
    if (downloadFilesFlag) {
      val downloadMap = TrieMap[String, List[String]]()
      val fileDownloader = FileDownloader()
      val path = "download"
      val downloadExecutor =
        ExecutionContext.fromExecutorService(Executors.newFixedThreadPool(4))
      try {
        Await.result(
          Future.sequence(apkMap.map {
            case (k, v) =>
              Future {
                crawler
                  .downloadFile(fileDownloader, path, k, v, downloadMap)
              }(
                downloadExecutor
              )
          }),
          1 hour
        )
      } finally {
        downloadExecutor.shutdown()
      }
      println(downloadMap)
    }
  }
}
