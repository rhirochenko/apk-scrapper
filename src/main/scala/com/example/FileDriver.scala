package com.example

import scala.collection.concurrent.TrieMap
import scala.io.Source
import java.io.{
  BufferedWriter,
  File,
  FileInputStream,
  FileNotFoundException,
  FileOutputStream,
  FileWriter,
  IOException,
  ObjectInputStream,
  ObjectOutputStream
}
import java.nio.file.Files

object FileDriver {
  def apply(): FileDriver = new FileDriver()
}

/**
  *  A class responsible for file read and write operations
  */
class FileDriver {

  /** Read text file from the disk and parse result to TrieMap[String, String] */
  def readTextFile(filePath: String): TrieMap[String, String] = {
    try {
      val file = Source.fromFile(filePath).getLines()
      val pairs =
        for {
          line <- file
          split = line.split(",").map(_.trim).toList
          buttonAppUrl = split.head
          downloadUrl = split.tail.mkString
        } yield (buttonAppUrl -> downloadUrl)
      pairs.to(TrieMap)
    } catch {
      case _: FileNotFoundException =>
        createFile(filePath)
        new TrieMap[String, String]()
      case e =>
        e.printStackTrace()
        new TrieMap[String, String]()
    }
  }

  /** Write TrieMap[String, String] to text file on the disk */
  def writeTextFile(
      filePath: String,
      t: TrieMap[String, String]
  ) = {
    val tempDir = Files.createTempDirectory("some-prefix").toString
    val tempFilePath = s"$tempDir/temp_apk_list.txt"

    val bw = new BufferedWriter(new FileWriter(tempFilePath, true))
    try {
      t.map {
        case (key: String, value: String) => {
          bw.write(s"$key, $value")
          bw.newLine()
        }
      }
      bw.close
      deleteFile(filePath)
      renameFile(tempFilePath, filePath)
    } catch {
      case e: IOException =>
        deleteFile(tempFilePath)
        e.printStackTrace()
    }
  }

  /** Create new file */
  def createFile(filePath: String): Unit = {
    val file = new File(filePath)
    try {
      file.createNewFile()
    } catch {
      case e: IOException =>
        e.printStackTrace()
        System.exit(1)
    }
  }

  /** Write serializable object to the disk */
  def writeSerializedObject[T <: Serializable](
      obj: T,
      filePath: String
  ): Unit = {
    val oos = new ObjectOutputStream(new FileOutputStream(filePath))
    oos.writeObject(obj)
  }

  /** Read serializable object from the disk */
  def readSerializedObject[T <: Serializable](filePath: String): Option[T] = {
    try {
      val ois = new ObjectInputStream(new FileInputStream(filePath))
      val obj = ois.readObject.asInstanceOf[T]
      ois.close()
      return Some(obj)
    } catch {
      case e: Exception =>
        e.printStackTrace()
        None
    }
  }

  /** Delete file from the disk */
  def deleteFile(filePath: String): Unit = {
    try {
      new File(filePath).delete()
    } catch {
      case e: Exception =>
        e.printStackTrace()
    }
  }

  /** Rename file on the disk */
  def renameFile(oldFilePath: String, newFilePath: String): Unit = {
    try {
      val oldFile = new File(oldFilePath)
      val newFile = new File(newFilePath)
      oldFile.renameTo(newFile)
    } catch {
      case e: Exception =>
        e.printStackTrace()
    }
  }
}
