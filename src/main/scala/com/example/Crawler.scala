package com.example

import com.typesafe.scalalogging.LazyLogging
import net.ruippeixotog.scalascraper.browser.{Browser, JsoupBrowser}
import net.ruippeixotog.scalascraper.scraper.ContentExtractors.{
  attr,
  elementList
}
import net.ruippeixotog.scalascraper.dsl.DSL._
import net.ruippeixotog.scalascraper.dsl.DSL.Extract._

import scala.language.postfixOps
import scala.collection.concurrent.TrieMap
import scala.collection.mutable.ArrayBuffer
import net.ruippeixotog.scalascraper.model.Document

object Crawler extends LazyLogging {
  def apply(baseUrl: String, maxNum: Int): Crawler =
    new Crawler(baseUrl, maxNum)
}

/**
  * A recursive crawler for the download URls search.
  *
  * @param baseUrl a URL for starting crawler to search
  * @param maxNum a number download URLs to search
  */
class Crawler(
    var baseUrl: String,
    var maxNum: Int
) extends LazyLogging {

  /** Сheck a page for a download link */
  def isApkUrl(apkUrl: String): Boolean = {
    apkUrl.contains("download.php")
  }

  /**
    * Check page contains download button
    *
    * @param html a Jsoun Document parsed html object
    * @return boolean result with
    */
  def checkDownloadButton(html: Document): Boolean = {
    val downloadButton = html >> elementList("a.btn.btn-flat.downloadButton")
    downloadButton
      .flatMap(x => x >> texts("a"))
      .filter(x => x.toLowerCase.contains("download apk"))
      .nonEmpty
  }

  /**
    * Get download link from button 'Download APK'.
    *
    * @param url a URL of application page with download button
    * @param browser a JsoupBrowser object
    * @return a download link if it was found
    */
  def getDownloadLink(url: String, browser: JsoupBrowser): Option[String] =
    try {
      val html = browser.get(url)
      val downloadButton = html >> elementList("a.btn.btn-flat.downloadButton")
      val link: Option[String] = downloadButton
        .map(x => x >?> attr("href"))
        .flatMap(x => x)
        .filter(x => !x.contains("javascript:void(0)") && !x.contains("http"))
        .map(x => baseUrl + x)
        .headOption

      link match {
        case Some(l) => {
          val downloadHtml = browser.get(l)
          val clickHereLink = downloadHtml >> elementList("a[rel=nofollow]")
          val downloadLink: Option[String] = clickHereLink
            .map(x => x >?> attr("href"))
            .flatMap(x => x)
            .filter(x =>
              !x.contains("javascript:void(0)") && !x.contains("http")
            )
            .map(x => baseUrl + x)
            .headOption
          downloadLink
        }
        case None => None
      }
    } catch {
      case e: Exception =>
        e.printStackTrace()
        None
    }

  /**
    * Get all links from page. The method parsing only main content div on the page for faster crawler convergence
    *
    * @param html a Jsoun Document parsed html object
    * @return a list of all found links from page
    */
  def getAllLinks(html: Document): List[String] = {
    val urls =
      List(
        html >> elementList("#content .table-cell.rowheight a"),
        html >> elementList("#primary a")
      )
    val urlsResult = urls
      .flatMap(x => x)
      .map(x => x >?> attr("href"))
      .flatMap(x => x)
      .filter(x => !x.contains("javascript:void(0)") && !x.contains("http"))
      .filter(x =>
        x.contains("/page") || x.contains("/apk") || x.contains("download")
      )
      .map(x => baseUrl + x)
    urlsResult
  }

  /**
    * Update all download links in the shared concurrent map. The method launches in the background
    * thread and exits when target number parsed links is collected.
    *
    * @param apkMap TrieMap[String, String] that contains application and download URLs
    * @param sleepTime a time of restarting method (in milliseconds)
    * @param fileDriver responsible for file disk operations
    * @param filename the text file with temporary results
    */
  def updateDownloadLinks(
      apkMap: TrieMap[String, String],
      sleepTime: Long,
      fileDriver: FileDriver,
      filename: String
  ): Unit = {
    while (apkMap.size < maxNum) {
      Thread.sleep(sleepTime)
      val browser = new JsoupBrowser()
      apkMap.foreach {
        case (downloadButtonUrl: String, downloadUrl: String) =>
          if (downloadUrl.isEmpty) {
            Thread.sleep(100)
            logger.info(s"Updating donwload link for $downloadButtonUrl")
            getDownloadLink(downloadButtonUrl, browser) match {
              case Some(x) => apkMap(downloadButtonUrl) = x
              case None    => apkMap(downloadButtonUrl) = ""
            }
          }
      }
      logger.info(s"Updated download links")
    }
    fileDriver.writeTextFile(filename, apkMap)
  }

  /**
    * Save temporary result from trie map to text file. The method launches in the background
    * thread and exits when target number parsed links is collected.
    *
    * @param filename the text file with temporary results
    * @param apkMap TrieMap[String, String] that contains application and download URLs
    * @param fileDriver responsible for file disk operations
    * @param sleepTime a time of restarting method (in milliseconds)
    */
  def saveApkMap(
      filename: String,
      apkMap: TrieMap[String, String],
      fileDriver: FileDriver,
      sleepTime: Long
  ): Unit = {
    while (apkMap.size < maxNum) {
      Thread.sleep(sleepTime)
      fileDriver.writeTextFile(filename, apkMap)
      logger.info(s"Updated apk file: $filename")
    }
  }

  /**
    * Recursively crawling URLs.
    *
    * @param url start URL for starting crawling
    * @param crawledUrls shared concurrent TrieMap[String, Boolean] that contains all visited URLs by crawlers
    * @param apkMap shared concurrent TrieMap[String, String] that contains application and download URLs
    * @return depth on which recursion exit
    */
  def crawlUrls(
      url: String,
      crawledUrls: TrieMap[String, Boolean],
      apkMap: TrieMap[String, String]
  ): Int = {
    def crawlUrlsHelper(
        browser: Browser,
        url: String,
        urlsList: ArrayBuffer[String],
        crawledUrls: TrieMap[String, Boolean],
        apkLinks: TrieMap[String, String],
        depth: Int,
        retryRate: Int
    ): Int = {
      // Coefficient for increasing time between request for avoiding 429 HTTP error
      var retry = retryRate

      // Check for maximum URLs depth for crawler to go
      if (depth > 5) {
        return depth
      }

      // Check if some crawler already visited URL
      if (!crawledUrls.contains(url)) {
        crawledUrls.addOne(url, false)
      } else {
        return depth
      }

      // Parse the page
      try {
        Thread.sleep(100 * retry)
        logger.info(
          s"Total found: ${apkLinks.size} Depth: $depth Check: $url, retryRate $retry"
        )
        val html = browser.get(url)
        retry = 1

        val urlsResult = getAllLinks(html)

        if (checkDownloadButton(html) && !apkLinks.contains(url)) {
          logger.info(s"Found one: $url, Total found: ${apkLinks.size}")
          apkLinks.addOne(url, "")
        }

        for (x <- urlsResult) {
          if (!urlsList.contains(x) && !crawledUrls.contains(x)) {
            urlsList.addOne(x)
          }
        }
      } catch {
        case e: Exception => {
          crawledUrls.remove(url)
          e.printStackTrace()
          Thread.sleep(1000 * retry)
          retry += 1
        }
      }

      // Recursively parse each link
      val urlsSet = urlsList.toSet
      for (url <- urlsSet) {
        if (apkLinks.size > maxNum) {
          return depth
        } else {
          crawlUrlsHelper(
            browser,
            url,
            urlsList,
            crawledUrls,
            apkLinks,
            depth + 1,
            retry
          )
        }
      }
      depth
    }

    val browser = new JsoupBrowser()

    crawlUrlsHelper(
      browser,
      url,
      ArrayBuffer[String](),
      crawledUrls,
      apkMap,
      depth = 0,
      retryRate = 1
    )
  }

  /**
    * Download file function.
    *
    * @param fileDownloader object responsible for the file downloading operations
    * @param path a target file path
    * @param appPageUrl a application URL with download button
    * @param downloadUrl a download URL
    * @param downloadMap a map for tracking downloaded file
    */
  def downloadFile(
      fileDownloader: FileDownloader,
      path: String,
      appPageUrl: String,
      downloadUrl: String,
      downloadMap: TrieMap[String, List[String]]
  ): Unit = {
    if (!downloadMap.contains(appPageUrl) && downloadMap.size < 10) {
      fileDownloader.parseFilename(appPageUrl) match {
        case Some(name) =>
          fileDownloader.downloadFile(downloadUrl, path, name)
          fileDownloader.fileInfo(path, name) match {
            case Some(fileInfo) =>
              logger.info(s"Downloaded $appPageUrl $fileInfo")
              downloadMap.addOne(appPageUrl, fileInfo)
            case None =>
              logger.trace(
                s"FileDownloader: fileinfo function failed for  $appPageUrl"
              )
          }
        case None =>
          logger.trace(
            s"FileDownloader: parseFilename function failed for  $appPageUrl"
          )
      }
    }
  }
}
