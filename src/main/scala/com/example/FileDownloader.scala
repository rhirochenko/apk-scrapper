package com.example

import java.io.File
import java.net.URL

import org.apache.commons.io.{FileUtils, FilenameUtils}

import scala.language.postfixOps

object FileDownloader {
  def apply(): FileDownloader = new FileDownloader()
}

/**
  * A class responsible for file downloading.
  */
class FileDownloader {

  /** Get file info in format filename - extension - filesize */
  def fileInfo(path: String, filename: String): Option[List[String]] = {
    val file = new File(s"$path/$filename")
    getFileSizeMegaBytes(file) match {
      case Some(s) =>
        Some(
          List(
            filename,
            FilenameUtils.getExtension(filename),
            s"${s.toString} MB"
          )
        )
      case None => None
    }
  }

  /** Download link function */
  def downloadFile(
      url: String,
      path: String,
      filename: String
  ): Option[Double] = {
    try {
      val file = new File(s"$path/$filename")
      FileUtils.copyURLToFile(new URL(url), file)
      getFileSizeMegaBytes(file)
    } catch {
      case e: Exception =>
        e.printStackTrace()
        None
    }
  }

  /** Get file size in megabytes */
  def getFileSizeMegaBytes(file: File): Option[Double] = {
    try {
      val fileSize = FileUtils.sizeOf(file)
      Some(fileSize / (1024 * 1024))
    } catch {
      case e: java.io.IOException =>
        e.printStackTrace()
        None
    }
  }

  /** Parse filename from application URL with 'Download APk' page */
  def parseFilename(appLink: String): Option[String] = {
    val name = appLink.split("/")
    name.lastOption match {
      case Some(n) => Some(s"$n.apk")
      case None    => None
    }
  }
}
