package com.example

import java.nio.file.Files

import org.scalatest.wordspec.AnyWordSpecLike

import scala.collection.concurrent.TrieMap

class FileDriverSpec extends AnyWordSpecLike {
  val tempDir = Files.createTempDirectory("some-prefix")
  val tempDirPath = tempDir.toString

  "FileWrite" must {

    "write TrieMap to txt file and read from txt file" in {
      val t = TrieMap[String, String]()
      val link1 = "http://www.google.ru"
      val link2 = "http://www.ya.ru"
      t.addOne(link1, "")
      t.addOne(link2, "http://www.ya.ru/download.php")

      val fileDriver = new FileDriver()
      val filePath = s"$tempDirPath/test.txt"
      fileDriver.writeTextFile(filePath, t)

      val readMap = fileDriver.readTextFile(filePath)
      assert(readMap == t)
    }

    "rewrite TrieMap to txt file and read from txt file" in {
      val fileDriver = new FileDriver()
      val filePath = s"$tempDirPath/test.txt"
      var t: TrieMap[String, String] = fileDriver.readTextFile(filePath)
      t.addOne("http://example.com", "")

      fileDriver.writeTextFile(filePath, t)
      val readMap = fileDriver.readTextFile(filePath)
      assert(readMap == t)
    }

    "write,read and overwrite serialized triemap" in {
      val t = TrieMap[String, String]()
      val link1 = "http://www.google.ru"
      val link2 = "http://www.ya.ru"
      t.addOne(link1, "")
      t.addOne(link2, "http://www.ya.ru/download.php")

      val fileDriver = new FileDriver()
      val filePath = s"$tempDirPath/crawledMap.obj"
      fileDriver.writeSerializedObject[TrieMap[String, String]](t, filePath)

      val readSerializedMap =
        fileDriver.readSerializedObject[TrieMap[String, String]](filePath)
      assert(readSerializedMap == Some(t))

      t.addOne("http://example.com", "")
      fileDriver.writeSerializedObject[TrieMap[String, String]](t, filePath)
      val readSerializedMapUpdated =
        fileDriver.readSerializedObject[TrieMap[String, String]](filePath)
      assert(readSerializedMapUpdated == Some(t))
    }
  }
}
