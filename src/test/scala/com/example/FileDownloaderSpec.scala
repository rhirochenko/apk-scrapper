package com.example

import java.nio.file.Files

import org.scalatest.wordspec.AnyWordSpecLike

class FileDownloaderSpec extends AnyWordSpecLike {
  val fileDownloader = FileDownloader()
  val tempDir = Files.createTempDirectory("some-prefix")
  val tempDirPath = tempDir.toString

  "FileDownloader" must {
    "parse app page url for file name" in {
      val appPageUrl =
        "https://www.apkmirror.com/apk/hbo-now/hbo-now-hbo-now/hbo-now-hbo-now-19-0-1-157-release/hbo-now-stream-tv-movies-19-0-1-157-android-apk-download/"
      val filename =
        "hbo-now-stream-tv-movies-19-0-1-157-android-apk-download.apk"
      assert(fileDownloader.parseFilename(appPageUrl) == Some(filename))
    }

    "download file" in {
      val appPageUrl =
        "https://www.apkmirror.com/apk/steelworks/apk-editor-steelworks/apk-editor-steelworks-1-6-10-release/apk-editor-1-6-10-android-apk-download/"
      val downloadUrl =
        "https://www.apkmirror.com/wp-content/themes/APKMirror/download.php?id=127165"
      val filename = fileDownloader.parseFilename(appPageUrl) match {
        case Some(f) => f
        case None    => fail("Error with parsing file")
      }
      val downloadResult =
        fileDownloader.downloadFile(downloadUrl, tempDirPath, filename)
      assert(downloadResult == Some(6.0))
    }

    "return info about downloaded file in format file-type-size" in {
      val filename =
        "apk-editor-1-6-10-android-apk-download.apk"
      assert(
        fileDownloader.fileInfo(tempDirPath, filename) ==
          Some(
            List(
              "apk-editor-1-6-10-android-apk-download.apk",
              "apk",
              "6.0 MB"
            )
          )
      )
    }
  }
}
