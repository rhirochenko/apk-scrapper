package com.example

import net.ruippeixotog.scalascraper.browser.JsoupBrowser
import org.scalatest.wordspec.AnyWordSpecLike
import com.example.Crawler._

class CrawlerSpec extends AnyWordSpecLike {
  val baseUrl: String = "https://www.apkmirror.com"
  val maxNum: Int = 1000
  val crawler = Crawler(baseUrl, maxNum)

  "Parser object" must {
    "find download button on app page" in {
      val appUrl =
        "https://www.apkmirror.com/apk/google-inc/storage-manager/storage-manager-r-release/smart-storage-r-android-apk-download/"
      val browser = JsoupBrowser()
      val html = browser.get(appUrl)
      assert(crawler.checkDownloadButton(html))
    }

    "not find download button on not app page" in {
      val appUrl =
        "https://www.apkmirror.com/apk/google-inc/storage-manager/storage-manager-r-release/"
      val browser = JsoupBrowser()
      val html = browser.get(appUrl)
      assert(!crawler.checkDownloadButton(html))
    }

    "return Some(link) for get download link" in {
      val appPageUrl =
        "https://www.apkmirror.com/apk/adguard-software-limited/adguard/adguard-3-4-54-release/"
      val browser = new JsoupBrowser()
      val link = crawler.getDownloadLink(appPageUrl, browser)

      val expectedResult = Some(
        "https://www.apkmirror.com/wp-content/themes/APKMirror/download.php?id=996368"
      )
      assert(link == expectedResult)
    }

    "return None get download link for non app page" in {
      val notAppPageUrl =
        "https://www.apkmirror.com/apk/adguard-software-limited/adguard"
      val browser = new JsoupBrowser()
      val link = crawler.getDownloadLink(notAppPageUrl, browser)
      assert(link == None)
    }
  }
}
