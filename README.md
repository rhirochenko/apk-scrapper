# Recursive crawler for parsing download links

Crawler is based on APKMirror website. It recursively searches pages with 'Download APK' button and append it to text file with results.
In a background task it open app page (with 'Download APK'), follow by this link and parse direct download link to text file with results.
If the flag 'downloadFilesFlag' is set to true, it will download 'apk' files after target number of links will be found.

### Requirements
 - Scala: v2.13.1
 - JDK 8+

### Run app
```
sbt run
```

### Run tests
```
sbt test
```
