name := "apk-scrapper"

version := "0.1"

scalaVersion := "2.13.1"

libraryDependencies ++= Seq(
  "net.ruippeixotog" %% "scala-scraper" % "2.2.0",
  "org.scalatest" %% "scalatest" % "3.1.0" % Test,
  "ch.qos.logback" % "logback-classic" % "1.2.3",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2"
)
